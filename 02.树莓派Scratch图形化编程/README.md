

# 树莓派Scratch编程

1. 参考上一章节打开远程桌面
2. 点击在桌面的左上角，树莓派标志，下拉菜单programming-->Scratch3，打开Scratch
   ![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01OMhejD1LbgbJG5l9N_!!63891318.png)
3. 在软件的左上角地球标志可以修改成中文界面
   ![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01Foudad1LbgbN4yRR1_!!63891318.png)
4. 接下来就要到了熟悉的使用环节了，为什么说熟悉呢？因为以前录制过Scratch的教程，在Arduino的图形化界面教程上，如果不熟悉的可以去学习ARDUINO教程中的Scratch，一样操作的。
   https://www.bilibili.com/video/av88711386
5. 以下教程只是简单测试下是否好使
6. 首先我们要知道树莓派的pinout，可以通过以下网站直接看
   https://pinout.xyz
7. Scratch左下角增加树莓派的板子积木
   ![](https://img.alicdn.com/imgextra/i4/63891318/O1CN016INahh1LbgbMTeW3O_!!63891318.png)
8. 按照以下代码方式拼接代码
   ![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01lfiGzD1LbgbDJczkP_!!63891318.png)
9. 按照以下方式接一个外置LED
   ![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01tYLujY1LbgbLEW6zj_!!63891318.png)
10. 然后就看到树莓派控制的LED在1秒闪烁