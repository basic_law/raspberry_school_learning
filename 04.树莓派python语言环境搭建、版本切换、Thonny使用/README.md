# 4.python开发环境搭建、python切换版本、Thonny使用

树莓派Raspbian默认已经安装了python，使用python -V查看当前的python版本

我们发现是python2.7的，我们知道python2.7的官方已经停止维护了，所以我们要最好切换到python3.7去写代码

需要用以下2个指令切换python版本

```
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 100
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 150
```

如果想要切换会python2.7可以用以下指令

```
sudo update-alternatives --config python
```

![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01BOfE551LbgbOTHSIB_!!63891318.png)

然后我们运行python，测试个print一个helloworld，这个各种语言入门代码

![](https://img.alicdn.com/imgextra/i3/63891318/O1CN01v5fjXc1LbgbQTgbO4_!!63891318.png)

OK，python没问题了。

以后就可以开发python的程序了，和其它平台用法没什么区别。

这里我们想去用python同样去控制GPIO，谁让我是从单片机起步的，什么东西都想最后控制硬件才完美，

那么就去查下控制GPIO需要什么库吧，经过查询，发现需要一个RPI.GPIO库，那么先看看自带有没有

使用pip list查看默认都安装有什么库吧，最终发现自带的有RPI.GPIO，那就可以直接用了

![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01bENEF11LbgbJ8wKM6_!!63891318.png)

然后赶紧试试RPI.GPIO库吧

新建一个blink.py文件内容为

```
#!/usr/bin/python3
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(26, GPIO.OUT)
GPIO.output(26, GPIO.LOW)
blinks = 0
print('开始闪烁')
while (blinks < 5):
    GPIO.output(26, GPIO.HIGH)
    time.sleep(1.0)
    GPIO.output(26, GPIO.LOW)
    time.sleep(1.0)
    blinks = blinks + 1
GPIO.output(26, GPIO.LOW)
GPIO.cleanup()
print('结束闪烁')
```

然后我们看到程序正常运行，LED也正常控制了

![](https://img.alicdn.com/imgextra/i3/63891318/O1CN01Gqsf2U1LbgbMnJ4Gs_!!63891318.png)

————————————————

LED连接方式同上一节，不再过多说明
![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01tYLujY1LbgbLEW6zj_!!63891318.png)



这个虽然能控制LED了，但是函数语法什么的都不熟悉，又想起昨天的wiringPi这个类似arduino的语法多好啊。python有没有对应的那，答案是必然的。而且发现这个库还是比原来的GPIO库要丰富很多，至少多了I2C等接口的支持，而且咱熟悉arduino啊，上手这个分分钟的

查到了官网

```
https://pypi.org/project/wiringpi/
```

github地址

```
https://github.com/WiringPi/WiringPi-Python
```

根据教程先pip安装wiringPi,

```
pip install wiringpi
```

如果出错，可以更换下国内源，用下面指令下载速度快点，不会出错

```
pip install wiringpi -i https://pypi.doubanio.com/simple
```

新建一个代码led.py测试下

```
import wiringpi

# One of the following MUST be called before using IO functions:
wiringpi.wiringPiSetup()      # For sequential pin numbering
# OR
# wiringpi.wiringPiSetupSys()   # For /sys/class/gpio with GPIO pin numbering
# OR
# wiringpi.wiringPiSetupGpio()  # For GPIO pin numbering

wiringpi.pinMode(25, 1)       # Set pin 26 to 1 ( OUTPUT )
while 1:
    wiringpi.digitalWrite(25, 1)  # Write 1 ( HIGH ) to pin 26
    wiringpi.delay(500)
    wiringpi.digitalWrite(25, 0)  # Write 0 ( LOW ) to pin 26
    wiringpi.delay(500)
	
```



然后，总觉得VI写代码好难受，然后又切回图形界面，发现自带一个Thonny的编辑环境，可以很容易写python，终于不用受vi的苦了！！

![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01cE3hRI1LbgbNffx9U_!!63891318.png)

具体这个Thonny的操作不再截图了，很简单。或者可以参考我的视频操作吧！

